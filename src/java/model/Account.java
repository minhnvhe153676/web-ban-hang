/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Quang Huy
 */
public class Account {

    private int userId;
    private String username;
    private String password;
    private String address;
    private String gmail;
    private String phone;
    private boolean isAdmin;
    private boolean isCustomer;

    public Account() {
    }

    public Account(int userId, String username, String password, String address, String gmail, String phone, boolean isAdmin, boolean isCustomer) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.address = address;
        this.gmail = gmail;
        this.phone = phone;
        this.isAdmin = isAdmin;
        this.isCustomer = isCustomer;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String gmail) {
        this.gmail = gmail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public boolean isIsCustomer() {
        return isCustomer;
    }

    public void setIsCustomer(boolean isCustomer) {
        this.isCustomer = isCustomer;
    }

    @Override
    public String toString() {
        return "Account{" + "userId=" + userId + ", username=" + username + ", password=" + password + ", address=" + address + ", gmail=" + gmail + ", phone=" + phone + ", isAdmin=" + isAdmin + ", isCustomer=" + isCustomer + '}';
    }

   
    

}
